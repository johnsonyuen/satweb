from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^help', views.help_page, name='help_page'),
    url(r'^privacy', views.privacy, name='privacy'),
    url(r'^new_order', views.new_order, name='new_order'),
    url(r'^contact', views.contact, name='contact'),
    url(r'^monitoring', views.monitor, name='monitor'),
    url(r'^about', views.about, name='about'),
    url(r'^staff', views.staff, name='staff'),
    url(r'^testimonials', views.testimonials, name='testimonials'),
    url(r'^services', views.services, name='services'),
    url(r'^$', views.index, name='index'),
]