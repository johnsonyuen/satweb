from django.shortcuts import render
from .forms import Enquiry_Form
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from .models import Enquiry
from django.core.mail import send_mail
from django.utils.html import strip_tags


# Create your views here.

def index(request):
    return render(request, 'SATWeb/index.html')


def help_page(request):
    return render(request, 'SATWeb/help_page.html')


def privacy(request):
    return render(request, 'SATWeb/privacy.html')


def monitor(request):
    return render(request, 'SATWeb/monitoring_product_page.html')


def about(request):
    return render(request, 'SATWeb/about_us.html')

def staff(request):
    return render(request, 'SATWeb/staff.html')

def services(request):
    return render(request, 'SATWeb/services.html')

def testimonials(request):
    return render(request, 'SATWeb/testimonials.html')

def new_order(request):
    if request.method == "POST":
        try:
            enquiry = Enquiry()
            enquiry.first_name = request.POST['first_name']
            enquiry.last_name = request.POST['last_name']
            enquiry.phone = request.POST['phone']
            enquiry.email = request.POST['email']
            enquiry.company = request.POST['company']
            if request.POST['site_coordinates'] is not None:
                enquiry.site_coordinates = request.POST['site_coordinates']


        except(KeyError):
            form = Enquiry_Form()
            return render(request, 'SATWeb/new_order.html', {'form': form, 'error_message':
                "You didn't select a choice."
                                                             })
        else:
            enquiry.save()
            email_new_enquiry(enquiry)

            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if a
            # user hits the Back button.
            return HttpResponseRedirect(reverse('index'))
    else:
        form = Enquiry_Form()
        return render(request, 'SATWeb/new_order.html', {'form': form})


def contact(request):
    return render(request, 'SATWeb/contact.html')


# Helper email method


def email_new_enquiry(enquiry):
    subject = 'New Enquiry from {0.first_name} {0.last_name}'.format(enquiry)

    html_message = 'You have received a new enquiry from {0.first_name} {0.last_name}.<br>' \
                   'First name: {0.first_name}<br>' \
                   'last_name: {0.last_name}<br>' \
                   'Phone: {0.phone}<br>' \
                   'Email: {0.email}<br>' \
                   'Company: {0.company}<br>'.format(enquiry)

    # provide some defaults for optional fields
    if enquiry.site_coordinates is not '':
        html_message + 'Site Co-ordinates: {0.site_coordinates}<br>'.format(enquiry)

    message = strip_tags(html_message)

    from_email = 'satweb@gmail.com'
    recipient_list = ['johnsonjsyuen@gmail.com']

    send_mail(subject, message, from_email, recipient_list, html_message)
