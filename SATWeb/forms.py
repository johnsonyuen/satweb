from django import forms
from .models import Enquiry
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from crispy_forms.bootstrap import TabHolder, Tab

class Enquiry_Form(forms.ModelForm):
    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Basic Information',
                'first_name',
                'last_name',
                'phone',
                'email',
                'company'
            ),
            Tab(
                'Detailed Information',
                'site_coordinates',
            )
        )
    )
    class Meta:
        model = Enquiry
        fields = ('first_name', 'last_name','phone','email','company','site_coordinates')



    # def __init__(self, *args, **kwargs):
    #     super(ExampleForm, self).__init__(*args, **kwargs)
    #     self.helper = FormHelper()
    #     self.helper.form_id = 'id-exampleForm'
    #     self.helper.form_class = 'blueForms'
    #     self.helper.form_method = 'post'
    #     self.helper.form_action = 'submit_survey'
    #
    #     self.helper.add_input(Submit('submit', 'Submit'))
