# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-30 11:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SATWeb', '0002_auto_20160130_1023'),
    ]

    operations = [
        migrations.AddField(
            model_name='enquiry',
            name='phone',
            field=models.PositiveIntegerField(default=2),
            preserve_default=False,
        ),
    ]
