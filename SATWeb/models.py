from __future__ import unicode_literals

from django.db import models


# Create your models here.


class Enquiry(models.Model):
    # basic info
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    phone = models.PositiveIntegerField()
    email = models.EmailField()
    company = models.CharField(max_length=20)

    # detailed info
    site_coordinates = models.CharField(blank=True,max_length=20)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)




