FROM python:latest

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*


#COPY requirements.txt ./
RUN git clone https://johnsonyuen@bitbucket.org/johnsonyuen/satweb.git  
WORKDIR /satweb
RUN pip install -r requirements.txt

EXPOSE 9000
CMD ["python", "manage.py", "runserver", "0.0.0.0:9000"]

#docker build -t sat/django .
#docker run --name sat --restart unless-stopped  -d -p 80:9000 sat/django
